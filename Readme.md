# Soda Crate (Läskbacken)

This is an old programming exercise that once haunted a friend of mine. It was the last exercise in a C# class.

You are given the task to model a crate for soda bottles ("läskback" in Swedish). The user can pick 24 bottles from a
catalog of different sodas. The total cost is calculated.

So the user should be able to do the obvious, add and remove bottles. But also sort the crate and get a random selection of sodas. Sorting can be on name, type or price.

The catalog of sodas can be searched, which in the case of a web interface may look a bit off, given the small catalog. Why not just list the whole catalog? But imagine that the catalog is much larger.

## Catalog

I do not have the original exercise, but I found a solution in a Gist. So I guess the catalog should look like:

| Name            | Type  | Price |
|-----------------|:-----:|------:|
| Coca-Cola       | Soda  | 14.95 |
| Mountain Dew    | Soda  | 14.95 |
| Fanta           | Soda  | 12.95 |
| Pepsi Max       | Soda  | 12.95 |
| Monster Energy  | Soda  | 17.95 |
| RedBull         | Soda  | 18.95 |
| Äppelmust       | Cider | 12.50 |
| Somersby Pär    | Cider | 12.50 |
| Somersby Fläder | Cider | 12.50 |
| Carlsberg       | Beer  | 16.95 |
| Heineken        | Beer  | 16.95 |
| Pripps Blå      | Beer  | 15.50 |
| Guinness        | Beer  | 18.50 |